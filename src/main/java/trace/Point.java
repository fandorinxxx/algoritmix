package trace;

public class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Point move(Point delta) {
        return new Point(
                this.x + delta.x,
                this.y + delta.y);
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (x != point.x) return false;
        return y == point.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString() {
        return String.format("[x:%2d, y:%2d]", x, y);
    }

    public static void main(String[] args) {
        Point p = new Point(3, 5);
        Point p2 = new Point(3, 5);
        System.out.println(p);
        System.out.println(p.equals(p2));
        Point p3 = p.move(new Point(1, 1));
        System.out.println(p3);
    }
}
